# Congestion Control Protocol

This project aims to design and implement a protocol for tracking and resolving network congestion. This is accomplished by detecting congestion, diagnosing the source and cause of congestion, and alerting the user's troubleshoot.

## Communication
  1. Open the terminal and run server.py
  2. Open a second terminal and run router.py
  3. Open a third terminal and run client.py
  4. Go back to the server terminal and type a message. Results will be displayed in the other 2 terminals. 
  
## Transmission
  1. run TransmissionRate.py
  2. Results will be displayed
