##############################################################################################
# Reference: https://medium.com/swlh/creating-a-simple-router-simulation-using-python-and-sockets-d6017b441c0909
################################################################################################

import socket

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # creates a socket that we’ll use to establish connections
server.bind(("localhost", 8000))  # bind((‘host name’ , ‘port number’ )) serves to bind the socket
server.listen(2)  # listen(number-of-queue-members) implies this socket takes connection requests from other sockets

IP_server = "25.15.15.02"
MAC_server = "00:A0:C9:14:C8:29"

MAC_router = "02:A4:F9:16:D8:59"

while True:
    connection, address = server.accept()
    # accept() identifies a connection request and returns the socket object and the address of the socket trying to
    # connect

    print("Connection from {address} established".format(address=address))
    if connection is not None:
        print(connection)
        break

while True:
    e_header = ""
    IP_header = ""

    print("\n Enter the message: ")
    message = raw_input()

    destination = "23.23.10.10"
    if destination == "23.23.10.10":
        src_ip = IP_server
        IP_header = IP_header + src_ip + destination

        src_mac = MAC_server
        dst_mac = MAC_router

        e_header = e_header + src_mac + dst_mac

        packet = e_header + IP_header + message
        packet = str(packet).encode("utf-8")
        connection.send(packet)  # sends some data to the connected client
    else:
        print("IP does not exist")
