# ############################################################################################################
# Reference: https://medium.com/swlh/creating-a-simple-router-simulation-using-python-and-sockets-d6017b441c09
# ############################################################################################################

import socket
import time

router = ("localhost", 8200)

user1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

time.sleep(1)
user1.connect(router)  # connect() tells User1 to try connect to the router

while True:
    received = (user1.recv(1024)).decode("utf-8")  # recv(1024) serves to receive any data the router might send
    mac_address_src = received[0:17]
    mac_address_dst = received[17:34]
    ip_address_src = received[34:45]
    ip_address_dst = received[45:56]
    message = received[56:]

    print("---------------------------------------------")
    print("The message is sent from ", ip_address_src)
    print("The message is - ", message)
    print("----------------------------------------------")
